﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour {


	// Game Controller reference
	private GameController _gameController;


	// Explosion animation objects for enemy objects (asteroids and enemy spaceships) and player
	// Asteroid and enemy spaceships explosion
	public GameObject explosion;
	// Asteroid and enemy spaceships explosion sound
	public AudioClip explosionSound;
	// Player spaceships explosion
	public GameObject playerExplosion;
	// Player spaceships explosion sound
	public AudioClip playerExplosionSound;
	// Reference to Sound FX audio source
	private GameObject _soundFXPlayer;

	// Score value that the player will get after destroy this game object
	public int scoreValue;


	// Use this for initialization
	void Start () {

		// Getting Game Controller reference used for sending game over message
		GameObject _gameControllerObject = GameObject.FindWithTag("GameController");
		_soundFXPlayer = GameObject.Find ("SoundFX");
		if (_gameControllerObject != null) {
			_gameController = _gameControllerObject.GetComponent<GameController>();
		} 
		if (_gameController	== null) {
			Debug.Log("Cannot find '_GameController' script");
		}
	}

	void OnTriggerEnter2D(Collider2D other) {

		// If the game object collides 
		if (
			// with boundaries,  
			(other.tag == "Boundary") ||
			// being an game object tagged as enemy collides with another enemy 
			(gameObject.tag == "Enemy" && other.tag == "Enemy") ||
			// or with  it will not be destroyed by this component
			(gameObject.name == "EnemyBolt(Clone)" && other.name == "Bolt(Clone)") 
		) {
			return;
		// If the game object collides with player, both will be destroyed and game will end
		} else if (other.tag == "Player") {
			Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
			_soundFXPlayer.GetComponent<AudioSource>().PlayOneShot(playerExplosionSound, PlayerPrefs.GetFloat("SoundFXVolume"));
			other.gameObject.SetActive (false);
			_gameController.transform.SendMessage ("LoseLife");

		// Any other collision will destroy it and get the score increased by the scoreValue
		} else {
			_gameController.AddScore(scoreValue);
			Destroy(other.gameObject);
		}

		Instantiate(explosion, transform.position, transform.rotation);
		_soundFXPlayer.GetComponent<AudioSource>().PlayOneShot(explosionSound, PlayerPrefs.GetFloat("SoundFXVolume"));

		Destroy(gameObject);
	}
}
