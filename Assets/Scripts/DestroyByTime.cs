﻿using UnityEngine;
using System.Collections;

public class DestroyByTime : MonoBehaviour {

	// Time in seconds that the game object will be alive
	public float lifetime;

	// Use this for initialization
	void Start () {
		Destroy(gameObject, lifetime);
	}
}
