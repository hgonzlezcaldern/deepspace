using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	// All hazards related variables
	// Hazards
	// Asteroids
	public GameObject hazard;
	// Enemy Spaceships
	public GameObject armedHazard;
	// Hazard spawn position
	public Vector3 spawnValues;
	// Hazards per wave
	public int hazardCount;
	// Time between hazards
	public float spawnWait;
	// Time til first wave is launched
	public float startWait;
	// Time til between waves
	public float waveWait;
	//Current lifes
	public int currentLifes;
	// Reference to player spaceship
	public GameObject spaceshipGO;
	private SpaceshipManager _spaceshipManager;

	// Flag that is used to check if the game is still on
	private bool _gameOver;
	// Reference to tha game over panel
	public GameObject panelGameOver;

	// All score related variables
	// Current score value
	public int score;
	// Size of top score list stored on player prefs
	private int _topScoreListSize;
	// Reference to Top Score List Text game object
	public GameObject topScoreListText;
	// Reference to Player Name Input Field game object
	public GameObject playerNameInputField;

	// Name of the current player
	private string _playerName;


	// Current level
	public int levelCount;
	// Speed of current level
	private float _levelSpeed ;


	void Start() {
		score = 0;

		_gameOver = false;
		_topScoreListSize = 10;
		_playerName = "";
		levelCount = 0;
		_levelSpeed = -5;

		_spaceshipManager = spaceshipGO.GetComponent<SpaceshipManager>();
		currentLifes = _spaceshipManager.GetTotalLifes();


		if (SceneManager.GetActiveScene ().name == "Level") {
			StartCoroutine(SpawnWaves());
		}

	}

	// Coroutine used to spawn hazards waves
	IEnumerator SpawnWaves() {
		Quaternion spawnRotation;
		Vector3 spawnPosition;
		float spawnDificultyFactor = 0.90f;


		yield return new WaitForSeconds(startWait);

		while (!_gameOver) {
			// Add 5 hazards every 2 levels
			hazardCount += (levelCount / 2) * 5;
			// Speed asteroids up by 0.5 every level
			_levelSpeed -= levelCount / 2;
			// Spawn hazards 10% every level
			spawnWait = spawnWait * spawnDificultyFactor;
			// 10% Hazard is armed, i.e. is an enemy spaceship
			for (int i = 0; i < hazardCount; i++) {
				if (Random.Range(0,199) < 9) {
					spawnPosition = new Vector3(Random.Range(-spawnValues.x,spawnValues.x), spawnValues.y, spawnValues.z);
					spawnRotation = Quaternion.identity;

					Instantiate(armedHazard, spawnPosition, spawnRotation);
				} else {
					spawnPosition = new Vector3(Random.Range(-spawnValues.x,spawnValues.x), spawnValues.y, spawnValues.z);
					spawnRotation = Quaternion.identity;
		
					hazard.GetComponent<Mover>().SetSpeed(_levelSpeed);
					Instantiate(hazard, spawnPosition, spawnRotation);
				}
				yield return new WaitForSeconds(spawnWait);
			}
			yield return new WaitForSeconds(waveWait);
			levelCount++;
			//Debug.Log("level: "+levelCount+" hazardCount: "+hazardCount+" hazardSpeed: "+hazard.GetComponent<Mover>().GetSpeed()+ " spawnWait: "+spawnWait);
		}
	}


	// Update current score value
	public void AddScore (int newScoreValue) {
		score += newScoreValue;
	}

	// Set current player name after player has entered it, hide input
	// field game object and show top score list	
	public void SetPlayerName () {
		_playerName = playerNameInputField.GetComponent<InputField>().text;
		SaveScore(_playerName, score);
		playerNameInputField.SetActive(false);
		topScoreListText.SetActive(true);
		UpdateTopScoreList();
	}

	// Check if score passed as parameter fit in top _topScoreListSize
	// and if it does stores it and update the list
	public void SaveScore(string name, int score) {
		int newScore;
		string newName;
		int oldScore;
		string oldName;
		newScore = score;
		newName = name;

		for (int i=0;i<_topScoreListSize;i++){
			if (PlayerPrefs.HasKey(i+"HScore")){
				if (PlayerPrefs.GetInt(i+"HScore")<newScore){ 
					// new score is higher than the stored score
					oldScore = PlayerPrefs.GetInt(i+"HScore");
					oldName = PlayerPrefs.GetString(i+"HScoreName");
					PlayerPrefs.SetInt(i+"HScore",newScore);
					PlayerPrefs.SetString(i+"HScoreName",newName);
					newScore = oldScore;
					newName = oldName;
				}
			} else {
				PlayerPrefs.SetString(i+"HScoreName",newName);
				PlayerPrefs.SetInt(i+"HScore",newScore);
				newScore = 0;
				newName = "";
			}
		}
	}

	// Reset Top Score List
	public void ResetTopScore() {
		int newScore = 0;
		string newName = "";

		for (int i=0;i<_topScoreListSize;i++){
			if (PlayerPrefs.HasKey(i+"HScore")){
				PlayerPrefs.SetString(i+"HScoreName",newName);
				PlayerPrefs.SetInt(i+"HScore",newScore);
			}
		}
	}

	// Check if score passed fit top _topScoreListSize
	public bool isTopScore(int score) {
		int newScore = score;

		for (int i=0;i<_topScoreListSize;i++){
			if (PlayerPrefs.HasKey(i+"HScore")){
				if (PlayerPrefs.GetInt(i+"HScore")<newScore){ 
					return true;
				}
			} 
		}
		return false;
	}

	// Return private attribute _playerName
	public string GetPlayerName() {
		name = _playerName;
		return name;
	}

	// Set _gameOver flag as true and display top score list and asks for
	// player names if needed, i.e.: player has beat the highscore
	public void GameOver() {
		_gameOver = true;

		if (isTopScore(score)) {
			playerNameInputField.SetActive(true);
		} else {
			topScoreListText.SetActive(true);
			UpdateTopScoreList();
		}
		ShowGameOver();
	}

	// Return private attribute _topScoreListSize
	public int GetTopScoreListSize() {
		return _topScoreListSize;
	}


	// Show game over panel
	public void ShowGameOver() {
		panelGameOver.SetActive (true);
	}

	// update top socre list panel
	public void UpdateTopScoreList() {
		for (int i=0;i<GetTopScoreListSize();i++){
			if (PlayerPrefs.HasKey(i+"HScore") && PlayerPrefs.GetInt(i+"HScore") != 0) {
				topScoreListText.GetComponent<Text>().text += PlayerPrefs.GetString(i+"HScoreName")+" "+PlayerPrefs.GetInt(i+"HScore")+"\n";
			}
		}
	}

	public void LoseLife() {
		Quaternion spawnRotation;
		Vector3 spawnPosition;


		currentLifes--;
		GameObject.Find ("Life_"+currentLifes).SetActive(false);
		Debug.Log(currentLifes);
		if (currentLifes == 0) {
			GameOver();
		} else {
			spawnPosition = spawnValues;
			spawnRotation = Quaternion.identity;

			spaceshipGO.GetComponent<Transform>().position = _spaceshipManager.initialPosition;

			spaceshipGO.SetActive(true);
			
			StartCoroutine(Blink(2.0f, spaceshipGO));


		}
	}

	IEnumerator Blink (float waitTime, GameObject go) {
		go.GetComponent<Collider2D>().enabled = false;
	    float endTime=Time.time + waitTime;
	    while(Time.time<endTime){
	        go.GetComponent<Renderer>().enabled = false;
	        yield return new WaitForSeconds(0.2f);
	        go.GetComponent<Renderer>().enabled = true;
	        yield return new WaitForSeconds(0.2f);
	    }
		go.GetComponent<Collider2D>().enabled = true;

	}
}


 
