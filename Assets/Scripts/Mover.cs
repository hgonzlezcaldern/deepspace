using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour {

	public float speed;
	private Rigidbody2D rigidbody2D;

	// Set game object to fowrward infinite move
	void Start() {
		rigidbody2D = GetComponent<Rigidbody2D>();
		rigidbody2D.velocity = transform.up * speed;
	}	

	public float GetSpeed() {
		return speed;
	}

	public void SetSpeed(float newSpeed) {
		speed = newSpeed;
	}
}
