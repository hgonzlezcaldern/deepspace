﻿using UnityEngine;
using System.Collections;

public class DestroyByBoundary : MonoBehaviour {

	// Destroy every game object that gets out of the game boundaries
	void OnTriggerExit2D(Collider2D other) {
		Destroy(other.gameObject);
	}
}
