﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class CanvasManager : MonoBehaviour {
	// Game Controller reference
	public GameController gameController;
	// Text Game object that displays current score 
	public Text textScore;
	// Text Game object that displays current level 
	public Text textLevel;
	// Heat bar 
	public Image heatBar;
	public GameObject spaceshipGO;
	private SpaceshipManager _spaceshipManager;

	// Initial score animation variables
	// Initial and default font size
	private int _fontSize;
	// Font size growth rate on adding score animation
	private int _growthRate;


	// Use this for initialization
	void Start () {
		_fontSize = 30;
		_growthRate = 1;
		_spaceshipManager = spaceshipGO.GetComponent<SpaceshipManager>();
	}

	// Every frame the function will check the current score value
	// and will update, if needed, the score text by one every frame
	// changing font size to greater values when updating and to 
	// lesser values when it has reached the current value.
	// Current level will also be checked and updated, but without any animation.
	void Update () {
		heatBar.GetComponent<RectTransform>().sizeDelta = new Vector2(heatBar.GetComponent<RectTransform>().sizeDelta.x,300 * _spaceshipManager.current_heat/_spaceshipManager.heating_limit);  //Scale(heatBar.GetComponent<RectTransform>().Scale.x,spaceshipGO.current_heat/spaceshipGO.heating_limit,heatBar.GetComponent<RectTransform>().Scale.z);
        float red = 0.5f;
        float green = 1.0f -_spaceshipManager.current_heat/_spaceshipManager.heating_limit;
        float blue = 0.0f ;
        float opacity = 1.0f;
        //Debug.Log("RGB: "+red+", "+green+", "+blue);
        heatBar.GetComponent<Image>().color = new Color(red, green, blue, opacity);
	}
	void FixedUpdate() {
		int displayScore = int.Parse(Regex.Replace(textScore.text, "[^0-9]", ""));
		int displayLevel = int.Parse(Regex.Replace(textLevel.text, "[^0-9]", ""));
		if (displayScore != gameController.score){
            if (textScore.fontSize != _fontSize + 10){
                    textScore.fontSize++;
            }
            if (displayScore < gameController.score){
                    displayScore += _growthRate;
            } else {
                    displayScore -= _growthRate;
            }
        } else if (textScore.fontSize != _fontSize){
                textScore.fontSize--;
        }

        if (displayLevel != gameController.levelCount + 1) {
        	displayLevel++;
       		textLevel.text = "Lvl" + displayLevel;
        }
       
        textScore.text = displayScore + "pts"; //Display the score to the user!

        
	}
}
