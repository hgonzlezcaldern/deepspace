using UnityEngine;
using System.Collections;

public class EnemySpaceshipManager : MonoBehaviour {

	// Movement
	private Rigidbody2D rigidbody2D;

	// Firing
	// Time when next shot will be fired
	private float nextFire;
	// Reference to shot prefab
	public GameObject shot;
	// Reference to shot spawn position
	public Transform shotSpawn;
	// Time between shots
	public float fireRate;
	// Fire sound
	public AudioClip fireSound;
	// Reference to Sound FX player
	private GameObject _soundFXPlayer;

	
	// Sets horizontal movement direction
	private int velocityXModifier = 1;
	// Frame counter for change on movement direction
	private int velocityFrame = 0;
	// Frame counter limit for change on movement direction
	private int velocityFrameLimit = 100;

	void Start () {
		// Get Sound FX player reference
		_soundFXPlayer = GameObject.Find ("SoundFX");
	}

	void Update() {
		// Auto fire
		if (Time.time >= nextFire) {
			nextFire = Time.time + fireRate;
			Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
			_soundFXPlayer.GetComponent<AudioSource>().PlayOneShot(fireSound, PlayerPrefs.GetFloat("SoundFXVolume"));
			GetComponent<AudioSource>().Play();

		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		// Change horizontal movement direction when frame limit reached
		velocityFrame = (velocityFrame+1)%velocityFrameLimit;
		if (velocityFrame == 0) {
			velocityXModifier = velocityXModifier * -1;
		}

		rigidbody2D = GetComponent<Rigidbody2D>();
		rigidbody2D.velocity = new Vector3(velocityXModifier*2f,rigidbody2D.velocity.y);
	}
}
