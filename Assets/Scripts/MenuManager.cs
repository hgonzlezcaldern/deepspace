﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuManager : MonoBehaviour {

	// Game Controller reference
	private GameController _gameController;

	// Audio related variables
	// Background audio
	private AudioSource _myAudioSource;
	private GameObject _soundFXPlayer;
	public AudioClip menuSoundFX;
	// Audio effect played on menu button press
	private AudioSource _myBGMusicSource;
	// Music volume slider game object reference
	public Slider musicVolumeSlider;
	// Sound FX volume slider game object reference
	public Slider soundFXVolumeSlider;
	// Confirmation panel game object reference
	public GameObject _popUpPanel;

	public void Start() {
		GameObject _gameControllerObject = GameObject.FindWithTag("GameController");
		if (_gameControllerObject != null) {
			_gameController = _gameControllerObject.GetComponent<GameController>();
		} 
		if (_gameController	== null) {
			//Debug.Log("Cannot find '_GameController' script");
		}

		_myAudioSource = GetComponent<AudioSource>();
		_soundFXPlayer = GameObject.Find ("SoundFX");


		// Check if there is any Sound FX Volume setting stored.
		// If there is set background music to that value,
		// if not set it to default value, 0.5f
		if (!PlayerPrefs.HasKey ("SoundFXVolume")) {
			PlayerPrefs.SetFloat ("SoundFXVolume", 0.5f);
		}
		AudioSource[] allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
		foreach (AudioSource audioSource in allAudioSources) { 
			//Debug.Log("E!"+ PlayerPrefs.GetFloat ("SoundFXVolume"));
			audioSource.volume=PlayerPrefs.GetFloat ("SoundFXVolume"); 
		} 
		// Check if there is any Music Volume setting stored.
		// If there is set background music to that value,
		// if not set it to default value, 0.5f
		if (!PlayerPrefs.HasKey ("MusicVolume")) {
			PlayerPrefs.SetFloat ("MusicVolume", 0.5f);
		} 
		_myAudioSource = GetComponent<AudioSource> ();
		_myAudioSource.volume = PlayerPrefs.GetFloat ("MusicVolume");
		if (GameObject.Find ("Music") != null) {
			_myBGMusicSource = GameObject.Find ("Music").GetComponent<AudioSource>();
			_myBGMusicSource.volume = PlayerPrefs.GetFloat ("MusicVolume");
		}

		// In addition if current scene is settings scene, set slider value
		// tha value.
		if (SceneManager.GetActiveScene ().name == "Settings") {
			musicVolumeSlider.value = PlayerPrefs.GetFloat ("MusicVolume");
			soundFXVolumeSlider.value = PlayerPrefs.GetFloat ("SoundFXVolume");
		}
	}

	// Change scene. If current scene is settings scene, save player preferences
	// before leaving
	public void ChangeScene(string sceneName) {

		if (SceneManager.GetActiveScene ().name == "Settings") {
			PlayerPrefs.SetFloat ("MusicVolume", musicVolumeSlider.value);
			PlayerPrefs.SetFloat ("SoundFXVolume", soundFXVolumeSlider.value);
		}

		//_myAudioSource.Play();
		_soundFXPlayer.GetComponent<AudioSource>().PlayOneShot(menuSoundFX, PlayerPrefs.GetFloat("SoundFXVolume"));
		SceneManager.LoadScene(sceneName);
	}

	public void ExitGame() {
		//_myAudioSource.Play();
		_soundFXPlayer.GetComponent<AudioSource>().PlayOneShot(menuSoundFX, PlayerPrefs.GetFloat("SoundFXVolume"));

		Application.Quit();
	}

	public void ResetTopScore() {
		_popUpPanel.SetActive(false);
		_gameController.transform.SendMessage ("ResetTopScore");

	}

	public void LaunchPopUpPanel() {
		_popUpPanel.SetActive(true);
	}

	public void HidePopUpPanel() {
		_popUpPanel.SetActive(false);
	}
}
