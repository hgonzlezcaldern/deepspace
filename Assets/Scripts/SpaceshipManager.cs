﻿using UnityEngine;
using System.Collections;

[System.Serializable ]
public class Boundary {
	public float xMin, xMax, yMin, yMax;
}

public class SpaceshipManager : MonoBehaviour {

	//Movement
	private float movex = 0f;
	private float movey = 0f;
	private Rigidbody2D rigidbody2D;
	public Boundary boundary;
	public float speed = 0f;
	public float tilt = 0f;
	public Vector3 initialPosition;

	//Firing
	private float nextFire;
	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate = 0.5f;
	public AudioClip fireSound;
	private GameObject _soundFXPlayer;
	public float heating_limit;
	public float heating_step;
	public float coolting_step;
	public float current_heat;
	private bool _heat_alarm;


	//Lifes
	public int totalLifes;
	
	void Start () {
		current_heat = 0;
		_heat_alarm = false;
		_soundFXPlayer = GameObject.Find ("SoundFX");
		initialPosition = GetComponent<Transform>().position;
	}

	void Update() {
//		Debug.Log("Heat: "+current_heat+ " Heat limit: "+heating_limit+ " Heat_alarm: "+_heat_alarm);
		if (Input.GetButton("Fire1") && Time.time >= nextFire && !_heat_alarm) {
			nextFire = Time.time + fireRate;
			current_heat = Mathf.Clamp(current_heat+heating_step,0.0f,heating_limit);
			if (current_heat == heating_limit) {
				_heat_alarm = true;
			}
			Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
			_soundFXPlayer.GetComponent<AudioSource>().PlayOneShot(fireSound, PlayerPrefs.GetFloat("SoundFXVolume"));
			GetComponent<AudioSource>().Play();

		} else if (!Input.GetButton("Fire1") || _heat_alarm) {
			current_heat = Mathf.Clamp(current_heat-coolting_step,0.0f,heating_limit);
			if (current_heat == 0.0f) {
				_heat_alarm = false;
			}
		}

	}

	// Update is called once per frame
	void FixedUpdate () {
		movex = Input.GetAxis ("Horizontal");
		movey = Input.GetAxis ("Vertical"); 

		Vector3 movement = new Vector3(movex, movey, 0.0f);
		rigidbody2D = GetComponent<Rigidbody2D>();
		rigidbody2D.velocity = movement * speed;

		rigidbody2D.position = new Vector3 (
			Mathf.Clamp(rigidbody2D.position.x, boundary.xMin, boundary.xMax),
			Mathf.Clamp(rigidbody2D.position.y, boundary.yMin, boundary.yMax),
			0.0f
		);


		transform.rotation = Quaternion.Euler(0.0f, rigidbody2D.velocity.x * -tilt, 0.0f);

	}

	public int GetTotalLifes() {
		return totalLifes;
	}
}
