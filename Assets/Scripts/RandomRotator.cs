﻿using UnityEngine;
using System.Collections;

public class RandomRotator : MonoBehaviour {

	public float tumble;

	// Use this for initialization
	// Set a random angular velocity to game object
	void Start () {
		GetComponent<Rigidbody2D>().angularVelocity = Random.value * tumble;
	}
	
}
