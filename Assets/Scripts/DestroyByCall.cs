﻿using UnityEngine;
using System.Collections;

public class DestroyByCall : MonoBehaviour {

	// Destroy the game object that call this function
	public void DestroyMe() {
		Destroy(gameObject);
	}
}
